# Nginx Load Balancing and using with Docker

<p align="center"><img src="1_H3U0Gud5ztpVTmkuVTwPoA.jpeg"></p>

> O balanceamento de carga para vários aplicativos é uma técnica comumente usada para otimizar a capacidade de resposta, a disponibilidade e a utilização de recursos. Os balanceadores de carga gerenciam o fluxo entre servidores e clientes. Eles distribuem o tráfego para diferentes servidores no pool de recursos para garantir que nenhum servidor fique sobrecarregado. Eles podem ser uma solução de hardware ou software.

> O Nginx pode ser usado como um ótimo balanceador de carga para distribuir o tráfego de entrada para os servidores e retornar as respostas do servidor selecionado para o cliente. O Nginx tem algumas vantagens do que outros balanceadores de carga;

- Ele suporta vários métodos de balanceamento que são ótimos para muitos casos
- Suporta cache estático e dinâmico
- Cada instância Nginx suporta totalmente vários aplicativos distintos
- Ele pode gerenciar a distribuição com base em cabeçalhos de solicitação dinâmicos, cookies, argumentos e até mesmo parâmetros de consulta
- Limitação de taxa, ponderação e gerenciamento de sessão

<p align="center"><img src="0_GeWMDIt-w7BdDn_r.png"></p>

O Nginx Open Source suporta quatro métodos de balanceamento de carga e o Nginx Plus suporta dois métodos adicionais.

- 1 - Round Robin: Este é o método padrão se você não especificar nada. Nesse método, uma solicitação será distribuída uniformemente aos serviços de back-end. Ou você pode especificar pesos a serem usados ​​na distribuição.
- 2 - Menos Conexões: Nesse método, a solicitação é enviada ao servidor com o menor número de conexões ativas naquele momento. Você também pode especificar pesos neste método.
- 3 - IP Hash: Nos outros métodos de balanceamento, todas as requisições dos clientes podem ser enviadas para diferentes servidores. Se você estiver usando uma sessão em seu aplicativo, não poderá fornecer persistência. Você tem duas opções nesta situação; você pode usar sessões compartilhadas como Redis entre seus servidores ou usar o método de hash IP no Nginx. Nesse método, o Nginx usa o endereço IP do cliente como chave e envia solicitações provenientes do mesmo IP para o mesmo servidor. Isso garante que o cliente seja sempre redirecionado para o mesmo servidor.
- 4 - Hash Genérico: Neste método, você pode usar suas próprias variáveis ​​para especificar que as solicitações serão enviadas para qual servidor. Essas variáveis ​​podem ser cabeçalho de solicitação, IP do cliente ou parâmetros de solicitação.

<p align="center"><img src="1_eaAaPaZB3cW3JUep6FFkFA.png"></p>

`❯ docker-compose up --scale api=3 -d`

> ou logo após subir, pode ser usar.

`❯ docker-compose scale api=3`

- k6 run -u 200 -d 30s — summary-export=export.json — out json=my_test_result.json script.js
- 
- https://levelup.gitconnected.com/nginx-load-balancing-and-using-with-docker-7e16c49f5d9
